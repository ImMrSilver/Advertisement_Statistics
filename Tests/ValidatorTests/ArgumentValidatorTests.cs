﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Advertisement_Statistics.Business;
using Advertisement_Statistics.Interfaces;

namespace Tests
{
    [TestClass]
    public class ArgumentValidatorTests
    {
        private IValidator _validator;

        public ArgumentValidatorTests() {
            _validator = new ArgumentValidator();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Three arguments(as file paths) have to be set")]
        public void ShouldThrowException_WhenArgumentCountIsWrong()
        {
            //Arrange
            string[] args = { "C:\\", "C:\\" };            
            //Act
            _validator.Validate(args);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "This is just a place to write some info about a test it does not validate the error msg")]
        public void ShouldThrowException_WhenFileDoesNotExist()
        {
            //Arrange
            string[] args = {
                "C:\\SomeRandomAgregatedFilesFolder\\",
                "C:\\SomeRandomAgregatedFilesFolder\\ViewableViews.csv",
                "C:\\SomeRandomAgregatedFilesFolder\\ViewableViews.csv"
            };
            //Act
            _validator.Validate(args);
        }
    }
}
