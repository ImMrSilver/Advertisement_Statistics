﻿using System;
using System.Collections.Generic;
using Advertisement_Statistics.Business;
using Advertisement_Statistics.Interfaces;
using Advertisement_Statistics.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Tests
{
    [TestClass]
    public class FileProcessingTests
    {
        private IFileProcessing _fileProcessing;
        private IFileReader _fileReader;
        private IFileWriter _fileWriter;
        private FileProcessingSettings _settings;
        private FileReaderSettings _ViewsSettings;
        private FileReaderSettings _ClicksSettings;
        private FileReaderSettings _VieableViewEventSettings;

        public FileProcessingTests() {

            SetupSettings();
            _fileWriter = MockRepository.GenerateMock<IFileWriter>();
            _fileReader = MockRepository.GenerateMock<IFileReader>();
            _fileProcessing = new FileProcessing(_settings, _fileReader, _fileWriter);
        }

        private void SetupSettings()
        {

            string ViewsFilePath = "C:\\AgregatedFiles\\Views.csv";
            string ViewableViewEventFilePath = "C:\\AgregatedFiles\\ViewableViews.csv";
            string ClicsFilePath = "C:\\AgregatedFiles\\Clicks.csv";

            _settings = new FileProcessingSettings()
            {
                StreamWindomInMinutes = 10,
                SlidingWindowLastLogTime = null,
                ViewsFilepath = ViewsFilePath,
                ViewableViewEventFilePath = ViewableViewEventFilePath,
                ClicksFilePath = ClicsFilePath,
                Take = 1000,
                Skip = 1
            };

            _VieableViewEventSettings = new FileReaderSettings
            {
                FilePath = _settings.ViewableViewEventFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };

            _ViewsSettings = new FileReaderSettings
            {
                FilePath = _settings.ViewsFilepath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };

            _ClicksSettings = new FileReaderSettings
            {
                FilePath = _settings.ClicksFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };
        }

        [TestMethod]
        public void CreateStatisticsShouldCallFileReaderReadRows()
        {
            var settings = new FileReaderSettings();
            //Arrange
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Is.Anything)).Return(new string[] { });
            //Act
            _fileProcessing.CreateAggregatedViewFiles();
            //Assert
            _fileReader.AssertWasCalled(x => x.ReadRows(Arg<FileReaderSettings>.Is.Anything));
        }

        [TestMethod]
        public void ReadingTheStreamShouldEndAfter_Getting0RowsFromGetViewSlidingWindow()
        {
            var totalFileReaderCallCount = 1;
            _settings.StreamWindomInMinutes = 10;
            //Arrange
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a => a.FilePath == _ClicksSettings.FilePath))).Return(new string[] { });
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a => a.FilePath == _ViewsSettings.FilePath))).Return(new string[] { });
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a => a.FilePath == _VieableViewEventSettings.FilePath))).Return(new string[] { });

            //Act
            _fileProcessing.CreateAggregatedViewFiles();
            //Assert
            _fileReader.AssertWasCalled(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a => a.FilePath == _ViewsSettings.FilePath)), options => options.Repeat.Times(totalFileReaderCallCount));
        }

        [TestMethod]
        public void GetSlidingWindowViews_ShouldReturnResultInCorrectTimeWindow()
        {
            _settings.StreamWindomInMinutes = 10;
            var views = new List<string>()
            {
                "5767776681449439088,2018-02-22 00:00:00.127,1221633",
                "5767776681449439088,2018-02-22 00:00:00.127,1221633",
                "8047815418162719221,2018-02-22 00:00:01.687,1232120",
                "3042367786416775494,2018-02-22 00:30:03.255,1188683"
            };
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _ViewsSettings.FilePath))).Return(views.ToArray());
            //Act
            var result = _fileProcessing.GetSlidingWindowViews();
            //Assert
            Assert.IsTrue(result[8047815418162719221].LogDate <= _settings.SlidingWindowLastLogTime);
        }

        [TestMethod]
        public void GetSlidingWindowClicks_ShouldReturnResultInCorrectTimeWindow()
        {

            _settings.SlidingWindowLastLogTime = DateTime.ParseExact("2018-02-22 00:19:09.674", "yyyy-MM-dd HH:mm:ss.fff",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var views = new List<string>()
            {
                "151925412000204915,2018-02-22 00:01:34.388,1232120,7443884296972096163",
                "151925424000383501,2018-02-22 00:03:38.516,1232120,6762670987676388834",
                "151925445000332906,2018-02-22 00:07:08.500,1049355,4447456238837908290",
                "151925451000278155,2018-02-22 00:08:08.624,1049355,7849994977622891877",
                "151925460000338755,2018-02-22 00:22:41.835,1049355,6118732578500926419"
            };
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _ClicksSettings.FilePath))).Return(views.ToArray());
            //Act
            var result = _fileProcessing.GetSlidingWindowClicks();
            //Assert
            Assert.IsTrue(result[7849994977622891877][0].LogDate <= _settings.SlidingWindowLastLogTime);
            Assert.IsTrue(!result.ContainsKey(6118732578500926419));
        }

        [TestMethod]
        public void GetSlidingWindowViewableViews_ShouldReturnResultInNextCorrectTimeWindow()
        {
            _settings.SlidingWindowLastLogTime = DateTime.ParseExact("2018-02-22 00:12:09.674", "yyyy-MM-dd HH:mm:ss.fff",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var viewableViewEvents = new List<string>()
            {
                "151925403000149618,2018-02-22 00:00:03.863,2975826529651648264",
                "151925403000315809,2018-02-22 00:00:07.216,4564671159070995313",
                "151925403000630610,2018-02-22 00:00:13.869,3565493259205775418",
                "151925403001706337,2018-02-22 00:16:24.915,4008981623713202978"             
            };

            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _VieableViewEventSettings.FilePath))).Return(viewableViewEvents.ToArray());

            //Act
            var result = _fileProcessing.GetSlidingWindowViewableViewEvents();
            //Assert
            Assert.IsTrue(result[3565493259205775418].LogDate <= _settings.SlidingWindowLastLogTime);
        }

    }
}
