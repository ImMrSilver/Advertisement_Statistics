﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Advertisement_Statistics.Business;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class SlidingWindowEvaluationTests
    {
        [TestMethod]
        public void SlidingWindowEvaluation_ShouldReturnRowCountIn2ndQuaterIfThereSlidingWindowEnds()
        {
            //Arrange
            var SlidingWindowDateTime = DateTime.Now;
            var rowCount = 1000;
            var dateTimeList = new List<DateTime>();

            for (var i = 0; i < rowCount; i++) {
                dateTimeList.Add(SlidingWindowDateTime.AddMinutes(i));
            }
         
            //So 3/4ths would be Current Datetime + 550 minutes later
            SlidingWindowDateTime = SlidingWindowDateTime.AddMinutes(550);

            //Act
            var result = SlidingWindowEvaluation.Get(dateTimeList.ToArray(),rowCount, SlidingWindowDateTime);

            //Assert
            Assert.AreEqual(500, result.Item2);
        }

        [TestMethod]
        public void SlidingWindowEvaluation_ShouldReturnFalseIfSlidingWindowDoesNotEndInTheCurrentDataSet()
        {
            //Arrange
            var SlidingWindowDateTime = DateTime.Now;
            var rowCount = 1000;
            var dateTimeList = new List<DateTime>();

            for (var i = 0; i < rowCount; i++)
            {
                dateTimeList.Add(SlidingWindowDateTime.AddMinutes(i));
            }

            //So next stream would be Current Datetime + 1562 minutes later
            SlidingWindowDateTime = SlidingWindowDateTime.AddMinutes(1562);

            //Act
            var result = SlidingWindowEvaluation.Get(dateTimeList.ToArray(), rowCount, SlidingWindowDateTime);

            //Assert
            Assert.AreEqual(false, result.Item1);
            Assert.AreEqual(0, result.Item2);
        }
    }
}
