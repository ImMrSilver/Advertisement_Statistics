﻿using Advertisement_Statistics.Business;
using Advertisement_Statistics.Interfaces;
using Advertisement_Statistics.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class StatisticsTests
    {
        private IStatistics _statistics;
        private IFileReader _fileReader;
        private IFileWriter _fileWriter;
        private StatisticsSettings _settings;
        private FileReaderSettings _viewabbleViewsSettings;
        private FileReaderSettings _viewsWithClicksSettings;

        public StatisticsTests() {

            SetupSettings();
            _fileWriter = MockRepository.GenerateMock<IFileWriter>();
            _fileReader = MockRepository.GenerateMock<IFileReader>();

            _statistics = new Statistics(_settings, _fileReader,_fileWriter);
        }

        private void SetupSettings() {

            string ViewsWithClicksFilePath = "C:\\AgregatedFiles\\ViewsWithClicks.csv";
            string ViewableViewsFilePath = "C:\\AgregatedFiles\\ViewableViews.csv";
            string StatisticsFilePath = "C:\\AgregatedFiles\\Statistics.csv";

            _settings = new StatisticsSettings()
            {
                StreamWindomInMinutes = 15,
                SlidingWindowLastLogTime = null,
                ViewsWithClicksFilePath = ViewsWithClicksFilePath,
                ViewableViewsFilePath = ViewableViewsFilePath,
                StatisticsFilePath = StatisticsFilePath,
                Take = 1000,
                Skip = 0
            };

            _viewabbleViewsSettings = new FileReaderSettings
            {
                FilePath = _settings.ViewableViewsFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };

            _viewsWithClicksSettings = new FileReaderSettings
            {
                FilePath = _settings.ViewsWithClicksFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };

        }

        [TestMethod]
        public void CreateStatisticsShouldCallFileReaderReadRows()
        {
            //Arrange 
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Is.Anything)).Return(new string[] { });       
            //Act
            _statistics.Create();
            //Assert
            _fileReader.AssertWasCalled(x => x.ReadRows(Arg<FileReaderSettings>.Is.Anything));
        }

        [TestMethod]
        public void ReadingTheStreamShouldEnd_AfterGetingZeroRows_FromFileReader()
        {
            //10 calls for _fileReader in GetSlidingWindowViewsWithClicks

            var totalFileReaderCallCount = 1;
            //Arrange
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a => a.FilePath == _viewabbleViewsSettings.FilePath))).Return(new string[] { });

            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a=>a.FilePath == _viewsWithClicksSettings.FilePath))).Return(new string[] { });
            //Act
            _statistics.Create();
            //Assert
            _fileReader.AssertWasCalled(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a => a.FilePath == _viewsWithClicksSettings.FilePath)), options => options.Repeat.Times(totalFileReaderCallCount));
        }

        [TestMethod]
        public void GetSlidingWindowViewsWithClicks_ShouldReturnACorrectViewsWithClicksDictionary()
        {
            var viewWithClicks = new List<string>()
            {
                "5767776681449439088,2018-02-22 00:00:00.127,",
                "8047815418162719221,2018 - 02 - 22 00:00:01.687,",
                "3042367786416775494,2018 - 02 - 22 00:00:03.255,",
                "5571915885765592918,2018 - 02 - 22 00:00:04.634,",
                "4564671159070995313,2018 - 02 - 22 00:00:05.815,",
                "4258062209306803268,2018 - 02 - 22 00:00:09.674,",
                "3565493259205775418,2018 - 02 - 22 00:00:11.240,",
                "175665289763121217,2018 - 02 - 22 00:00:16.502,",
                "6482015965814532506,2018 - 02 - 22 00:00:18.173,",
                "4340448803861105892,2018 - 02 - 22 00:00:29.037,9999999",
                "4340448803861105892,2018 - 02 - 22 00:16:30.037,5555555"
            };
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>.Matches(a => a.FilePath == _viewsWithClicksSettings.FilePath))).Return(viewWithClicks.ToArray());
            //Act
            var result = _statistics.GetSlidingWindowViewsWithClicks();
            //Assert
            Assert.AreEqual(10, result.Count);
            Assert.AreEqual(1, result[4340448803861105892].Count);//two clicks for the same view
        }

        [TestMethod]
        public void GetSlidingWindowViewsWithClicks_ShouldReturnResultInCorrectTimeWindow()
        {

            _settings.SlidingWindowLastLogTime = DateTime.ParseExact("2018-02-22 00:00:09.674", "yyyy-MM-dd HH:mm:ss.fff",
                                       System.Globalization.CultureInfo.InvariantCulture); 

            var viewWithClicks = new List<string>()
            {
                "5767776681449439088,2018-02-22 00:00:00.127,",
                "8047815418162719221,2018 - 02 - 22 00:00:01.687,",
                "3042367786416775494,2018 - 02 - 22 00:00:03.255,",
                "5571915885765592918,2018 - 02 - 22 00:00:04.634,",
                "4564671159070995313,2018 - 02 - 22 00:00:05.815,",
                "4258062209306803268,2018 - 02 - 22 00:00:09.674,",
                "3565493259205775418,2018 - 02 - 22 00:00:11.240,",
                "175665289763121217,2018 - 02 - 22 00:00:16.502,",
                "6482015965814532506,2018 - 02 - 22 00:00:18.173,",
                "4340448803861105892,2018 - 02 - 22 00:00:29.037,9999999",
                "4340448803861105892,2018 - 02 - 22 00:16:30.037,5555555"
            };
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _viewsWithClicksSettings.FilePath))).Return(viewWithClicks.ToArray());
            //Act
            var result = _statistics.GetSlidingWindowViewsWithClicks();
            //Assert
            Assert.IsTrue(result[4258062209306803268][0].LogDate <= _settings.SlidingWindowLastLogTime);
        }

        [TestMethod]
        public void GetSlidingWindowCampaignViews_ShouldReturnACorrectViewsWithClicksDictionary()
        {

            _settings.SlidingWindowLastLogTime = DateTime.ParseExact("2018-02-22 01:01:22.276", "yyyy-MM-dd HH:mm:ss.fff",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var campaignViews = new List<string>()
            {
                "5128084505566306549,2018 - 02 - 22 01:01:17.273,1199166,",
                "7398606504427646858,2018 - 02 - 22 01:01:21.161,1188683,",
                "7241611434986526932,2018 - 02 - 22 01:01:22.689,1020893,",
                "108000006263378974,2018 - 02 - 22 01:01:23.774,1020893,151925769000974427",
            };
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _viewabbleViewsSettings.FilePath))).Return(campaignViews.ToArray());
            //Act
            var result = _statistics.GetSlidingWindowCampaignViews();
            //Assert
            Assert.AreEqual(result.Count, 2);
        }

        [TestMethod]
        public void GetSlidingWindowCampaignViews_ShouldReturnResultInTheCorrectTimeWindow()
        {

            _settings.SlidingWindowLastLogTime = DateTime.ParseExact("2018-02-22 01:00:42.689", "yyyy-MM-dd HH:mm:ss.fff",
                                       System.Globalization.CultureInfo.InvariantCulture); ;

            var campaignViews = new List<string>()
            {
                "8466677170294295038,2018-02-22 01:00:42.118,1020893,151925766000393253",
                "2181474247375595712,2018 - 02 - 22 01:00:42.689,1188683,",
                "6415954291163322982,2018 - 02 - 22 01:00:44.141,1232120,",
                "2077633560293400015,2018 - 02 - 22 01:00:44.257,1232120,",
                "3759961490678164120,2018 - 02 - 22 01:00:44.621,1232120,",
                "4211341117036412767,2018 - 02 - 22 01:00:44.987,1221633,",
                "5231750163840331723,2018 - 02 - 22 01:00:45.750,1232120,",
                "2282611862119036442,2018 - 02 - 22 01:00:47.034,1232120,",
                "2763951314660323490,2018 - 02 - 22 01:00:48.264,1232120,",
                "6318735647638491915,2018 - 02 - 22 01:00:49.183,1221633,",
                "5951794667532176179,2018 - 02 - 22 01:00:51.579,1049355,",
                "7806219489442426555,2018 - 02 - 22 01:00:53.179,1221633,",
                "5128084505566306549,2018 - 02 - 22 01:01:17.273,1199166,",
                "7398606504427646858,2018 - 02 - 22 01:01:21.161,1188683,",
                "7241611434986526932,2018 - 02 - 22 01:01:22.689,1020893,",
                "108000006263378974,2018 - 02 - 22 01:01:23.276,1020893,151925769000974427",
            };
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _viewabbleViewsSettings.FilePath))).Return(campaignViews.ToArray());
            //Act
            var result = _statistics.GetSlidingWindowCampaignViews();
            //Assert
            Assert.IsTrue(result[1188683][0].LogDate <= _settings.SlidingWindowLastLogTime);
        }

        [TestMethod]
        public void WriteStatisticsToFileShouldCallFileWriter()
        {
            var settings = new FileReaderSettings();
            //Arrange
            _fileWriter.Expect(x => x.WriteRows(Arg<List<string>>.Is.Anything, Arg<string>.Is.Anything));

            var viewWithClicks = new List<string>()
            {
                "5767776681449439088,2018-02-22 00:00:00.127,1020893",
                "8047815418162719221,2018-02-22 00:00:01.687,1020893",
                "3042367786416775494,2018-02-22 00:00:03.255,",
                "5571915885765592918,2018-02-22 00:00:04.634,1020893",
                "4564671159070995313,2018-02-22 00:00:05.815,",
                "4258062209306803268,2018-02-22 00:00:09.674,",
                "3565493259205775418,2018-02-22 00:00:11.240,",
                "175665289763121217,2018-02-22 00:00:16.502,",
                "6482015965814532506,2018-02-22 00:00:18.173,",
                "4340448803861105892,2018-02-22 00:00:29.037,1020893",
                "4340448803861105892,2018-02-22 00:15:30.037,1221633"
            };
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _viewsWithClicksSettings.FilePath))).Return(viewWithClicks.ToArray());
            var viewsWithClicsDictionary = _statistics.GetSlidingWindowViewsWithClicks();

            var campaignViews = new List<string>()
            {
                "5767776681449439088,2018-02-22 00:00:00.127,1020893,151925766000393253",
                "2181474247375595712,2018-02-22 00:00:01.687,1020893,",
                "5571915885765592918,2018-02-22 00:00:03.255,1020893,",
                "2077633560293400015,2018-02-22 00:00:04.634,1020893,",
                "3759961490678164120,2018-02-22 00:00:05.815,1020893,",
                "4211341117036412767,2018-02-22 00:00:09.674,1020893,",
                "5231750163840331723,2018-02-22 00:00:11.240,1020893,",
                "2282611862119036442,2018-02-22 00:00:16.502,1020893,",
                "2763951314660323490,2018-02-22 00:00:18.173,1020893,",
                "6318735647638491915,2018-02-22 00:00:29.037,1020893,",
                "5951794667532176179,2018-02-22 00:00:30.037,1020893,",
                "7806219489442426555,2018-02-22 00:00:30.037,1221633,",
                "5128084505566306549,2018-02-22 00:00:30.037,1020893,",
                "7398606504427646858,2018-02-22 00:00:30.037,1020893,",
                "7241611434986526932,2018-02-22 00:00:30.037,1020893,",
                "1080000062633789741,2018-02-22 00:15:30.037,1020893,151925769000974427",
            };
 
            _fileReader.Expect(x => x.ReadRows(Arg<FileReaderSettings>
                .Matches(a => a.FilePath == _viewabbleViewsSettings.FilePath))).Return(campaignViews.ToArray());

            var statistics = new List<string>() {
                "---Statistics Window: 2018-02-22 00:00:00.127 - 00:15:00.127---",
                "1020893,14,2,1,14.2857",
                "1221633,1,0,0,0"
            };

            //Act
            var campaignViewDictionary = _statistics.GetSlidingWindowCampaignViews();
            //Act
            _statistics.WriteToFile(viewsWithClicsDictionary, campaignViewDictionary);
            //Assert
            _fileWriter.AssertWasCalled(x => x.WriteRows(Arg<List<string>>.Is.Equal(statistics), Arg<string>.Is.Equal(_settings.StatisticsFilePath)));
        }
    }
}
