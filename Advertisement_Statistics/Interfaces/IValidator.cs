﻿namespace Advertisement_Statistics.Interfaces
{
    public interface IValidator
    {
        void Validate(string[] args);
    }
}