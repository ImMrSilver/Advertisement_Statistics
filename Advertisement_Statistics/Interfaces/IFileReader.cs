﻿using Advertisement_Statistics.Models;

namespace Advertisement_Statistics.Interfaces
{
    public interface IFileReader
    {
        string[] ReadRows(FileReaderSettings settings);
        string[] ReadRowsFromStream(FileReaderSettings settings);
    }
}