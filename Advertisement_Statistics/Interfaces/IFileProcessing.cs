﻿using Advertisement_Statistics.Models;
using System.Collections.Generic;

namespace Advertisement_Statistics.Interfaces
{
    public interface IFileProcessing
    {
        void CreateAggregatedViewFiles();
        Dictionary<long, PartialView> GetSlidingWindowViews();
        Dictionary<long, List<Click>> GetSlidingWindowClicks();
        Dictionary<long, ViewableViewEvent> GetSlidingWindowViewableViewEvents();
    }
}