﻿using Advertisement_Statistics.Models;
using System.Collections.Generic;

namespace Advertisement_Statistics.Interfaces
{
    public interface IStatistics
    {
        void Create();
        Dictionary<long, List<ViewWithClicks>> GetSlidingWindowViewsWithClicks();
        Dictionary<int, List<CampaignViewStatistics>> GetSlidingWindowCampaignViews();
        void WriteToFile(Dictionary<long, List<ViewWithClicks>> slidingWindoViewsWithClicks,Dictionary<int, List<CampaignViewStatistics>> slidingWindowCampaignViewss);
    }
}