﻿using Advertisement_Statistics.Models;
using Advertisement_Statistics;
using Advertisement_Statistics.Business;
using System.Collections.Generic;

namespace Advertisement_Statistics.Interfaces
{
    public interface IFileWriter
    {
        void WriteRows(List<string> rows, string location);
    }
}