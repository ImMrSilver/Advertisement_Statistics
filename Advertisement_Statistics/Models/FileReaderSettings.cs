﻿namespace Advertisement_Statistics.Models
{
    public class FileReaderSettings
    {
        public string FilePath;
        public int Skip;
        public int Take;
        public bool NotallRecordsProccessedInWindow = true;
        public bool SlidingWindowEndsInCurrentDataSet = true;
        public bool EOF = false;
    }
}
