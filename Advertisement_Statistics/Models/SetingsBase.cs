﻿using System;

namespace Advertisement_Statistics.Models
{
    public class SettingsBase
    {
        public int StreamWindomInMinutes;
        public DateTime? SlidingWindowLastLogTime;
        public int Take;
        public int Skip;
    }
}
