﻿using System;

namespace Advertisement_Statistics.Models
{
    //used because of better performance
    public struct PartialView
    {
        public int CampaignId;
        public DateTime LogDate;
    }

    public struct Click
    {
        public long Id;
        public DateTime LogDate;
        public int CampaignId;
    }

    public struct ViewableViewEvent
    {
        public DateTime LogDate;
        public long ViewableViewEventId;
    }

    public struct CampaignViewStatistics
    {
        public DateTime LogDate;
        public long? ClickId;
        public long? ViewableViewId;
        public long ViewId;
    }

    public struct ViewWithClicks
    {
        public long Id;
        public DateTime LogDate;
        public long? ClickId;
    }
}
