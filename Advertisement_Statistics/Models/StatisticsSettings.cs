﻿namespace Advertisement_Statistics.Models
{
    public class StatisticsSettings: SettingsBase
    {
        public string ViewsWithClicksFilePath;
        public string ViewableViewsFilePath;
        public string StatisticsFilePath;
    }
}
