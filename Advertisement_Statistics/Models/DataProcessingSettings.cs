﻿using System;

namespace Advertisement_Statistics.Models
{
    public class FileProcessingSettings : SettingsBase
    {
        public string ViewsFilepath;
        public string ClicksFilePath;
        public string ViewableViewEventFilePath;
        public string ViewsWithClicksFilePath;
        public string ViewableViewsFilePath;
    }
}
