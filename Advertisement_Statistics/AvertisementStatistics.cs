﻿using Advertisement_Statistics.Business;
using Advertisement_Statistics.Interfaces;
using Advertisement_Statistics.Models;
using System;

namespace Advertisement_Statistics
{
    class AvertisementStatistics
    {
        static void Main(string[] args)
        {
            //Comment out for debugging
            IValidator _validator = new ArgumentValidator();
            _validator.Validate(args);

            string ViewsFilepath = args[0];
            string ClicksFilePath = args[1];
            string ViewableViewEventFilePath = args[2];

            //This output file directory will be created, if you have C drive
            string ViewsWithClicksFilePath = "C:\\AgregatedFiles\\ViewsWithClicks.csv";
            string ViewableViewsFilePath = "C:\\AgregatedFiles\\ViewableViews.csv";
            string StatisticsFilePath = "C:\\AgregatedFiles\\Statistics.csv";

            FileProcessingSettings fileProcessingSettings = new FileProcessingSettings()
            {
                StreamWindomInMinutes = 10,
                Skip = 1, //First line in test files is a column description so we skip that
                Take = 1000,
                SlidingWindowLastLogTime = null,
                ViewsFilepath = args[0],
                ClicksFilePath =args[1],
                ViewableViewEventFilePath = args[2],
                ViewsWithClicksFilePath = ViewsWithClicksFilePath,
                ViewableViewsFilePath = ViewableViewsFilePath
            };

            StatisticsSettings statisticsSettings = new StatisticsSettings()
            {
                StreamWindomInMinutes = 15,
                SlidingWindowLastLogTime = null,
                ViewsWithClicksFilePath = fileProcessingSettings.ViewsWithClicksFilePath,
                ViewableViewsFilePath = fileProcessingSettings.ViewableViewsFilePath,
                StatisticsFilePath = StatisticsFilePath,
                Take = 1000,
                Skip = 0
            };

            IFileReader fileReader = new FileReader();
            IFileWriter fileWriter = new FileWriter();
            IFileProcessing _processing = new FileProcessing(fileProcessingSettings, fileReader, fileWriter);
            _processing.CreateAggregatedViewFiles();
      
            IStatistics _statistics = new Statistics(statisticsSettings, fileReader, fileWriter);
            _statistics.Create();
            Console.ReadLine();
        }
    }

   
}
