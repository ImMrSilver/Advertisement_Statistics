﻿using Advertisement_Statistics.Interfaces;
using System;
using System.IO;

namespace Advertisement_Statistics.Business
{
    public class ArgumentValidator: IValidator
    {
        /* Created this ArgumentValidator to show that validations should be done on a separate layer,
         * in a separate file, because otherwise, some developers might do the validations in Controller,
         * Service, Repository.. anywhere, and when you will need to change the validation
         * it will be tricky an untidy
         * So it's better to have most of the validation logic in the Validator clases 
         */
        public void Validate(string[] args)
        {
            ValidateArgumentLength(args.Length);

            var ViewFilePath = args[0];
            var ClickFilePath = args[1];
            var ViewableViewEventPath = args[2];

            ValidateIfArgumentsAreExistingFiles(args);

            //Also you could validate the correct file type,
            //if all file paths are not the same etc..
        }

        private void ValidateIfArgumentsAreExistingFiles(string[] args)
        {
            foreach (var filePath in args) {
                try
                {
                    FileAttributes attr = File.GetAttributes(filePath);
                   
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    { 
                        //arg is a directory 
                        throw new FileNotFoundException();
                    }
                   
                }
                catch (FileNotFoundException ex) {
                    throw new ArgumentException("Arguments must be a correct path to an existing file");
                }

            }
        }

        private void ValidateArgumentLength(int lenght)
        {
            if (lenght != 3)
            {
                throw new ArgumentException("Three arguments(as file paths) have to be set");
            }
        }
    }
}