﻿using System;
using System.Globalization;

namespace Advertisement_Statistics.Common
{
   public static class DateTimeFormatting
    {
        public static string FormatDateTimeToCustom(DateTime dateTime)
        {
            var formattedDate = dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
            return formattedDate;
        }

        public static string FormatDateTimeToCustomOnlyTime(DateTime dateTime)
        {
            var formattedDate = dateTime.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture);
            return formattedDate;
        }

    }
}
