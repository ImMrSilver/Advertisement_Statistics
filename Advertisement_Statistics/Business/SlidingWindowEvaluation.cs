﻿using System;
using System.Linq;

namespace Advertisement_Statistics.Business
{
    /* Used in order to do as few DateTime comparisons from read stream for the SlidingWindowDataSet.
     * Now we can determine if f.e. sliding window ends in 3/4 th's of the row list without comparing all the dates.
     * In that case I don't have to do Datetime comparisons for f.e. the first 3/4 or 1/2 of the list
     * because any unnessesary dateTime comparisons, will reduce performance
     * F.E. we read 1000 rows, and sliding window closes in 753 row, so with this class we don't have to compare 
     * each row above 750 to determine where the slidingWindow ends
     * Static because I see it as a Math formula, just now its a very piritive one for now,
     * Depending on the element count, with least amount of comparisons, get closest row number to SlidingWindowDate.
     * won't change(having optimal algorythm)
     */
    public static class SlidingWindowEvaluation
    {
        public static (bool, int) Get(DateTime[] dateTimes, int rowCount, DateTime StreamWindowEndDate)
        {
            var slidingWindowIsInThisDataSet = false;
            //Check if we don't have to get a new stream
            if (dateTimes.LastOrDefault() >= StreamWindowEndDate)
            {
                slidingWindowIsInThisDataSet = true;
                int firstQuaterValue = rowCount / 4;
                if (dateTimes[firstQuaterValue] >= StreamWindowEndDate)
                {
                    return (slidingWindowIsInThisDataSet, 0);
                }

                int secondQuaterValue = rowCount / 2;
                if (dateTimes[secondQuaterValue] >= StreamWindowEndDate)
                {
                    return (slidingWindowIsInThisDataSet, firstQuaterValue);
                }

                int thirdQuaterValue = (rowCount/ 4)*3;
                if (dateTimes[thirdQuaterValue] >= StreamWindowEndDate)
                {
                    return (slidingWindowIsInThisDataSet, secondQuaterValue);
                }
                return (slidingWindowIsInThisDataSet, thirdQuaterValue);
            }
            
            //in this case slidingWindow has not ended in current stream
            return (false, 0);
        }

    }
}
