﻿using Advertisement_Statistics.Common;
using Advertisement_Statistics.Interfaces;
using Advertisement_Statistics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advertisement_Statistics.Business
{
    public class FileProcessing: IFileProcessing
    {
        private FileProcessingSettings _settings;
        private IFileReader _fileReader;
        private IFileWriter _fileWriter;
        private FileReaderSettings _viewFileReaderSettings;
        private FileReaderSettings _clickFileReaderSettings;
        private FileReaderSettings _viewableViewEventFileReaderSettings;

        public FileProcessing(
            FileProcessingSettings settings,
            IFileReader fileReader,
            IFileWriter fileWriter)
        {
            _fileReader = fileReader;
            _fileWriter = fileWriter;
            _settings = settings;
            SetFileReaderSettings();
        }

        private void SetFileReaderSettings()
        {
            _viewFileReaderSettings = new FileReaderSettings
            {
                FilePath = _settings.ViewsFilepath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };

            _clickFileReaderSettings = new FileReaderSettings
            {
                FilePath = _settings.ClicksFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };

            _viewableViewEventFileReaderSettings = new FileReaderSettings
            {
                FilePath = _settings.ViewableViewEventFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };
        }

        public void CreateAggregatedViewFiles()
        {
            Console.WriteLine("Creating ViewsWithClicks.csv and ViewableViews.csv files.");
            var fileContainsRows = !_viewFileReaderSettings.EOF;
            while (fileContainsRows)
            {
                var slidingWindowViews = GetSlidingWindowViews();
                fileContainsRows = !_viewFileReaderSettings.EOF;
                var slidingWindowClicks = GetSlidingWindowClicks();
                var slidingWindowWiewableViewEvents = GetSlidingWindowViewableViewEvents();
                if (slidingWindowViews.Count > 0)
                {
                    AggregateViewsWithClicks(slidingWindowViews, slidingWindowClicks);
                    AggregateViewableViews(slidingWindowViews, slidingWindowWiewableViewEvents);
                }
            }
            Console.WriteLine("ViewsWithClicks.csv and ViewableViews.csv created.");
        }

        private void AggregateViewsWithClicks(Dictionary<long, PartialView> slidingWindowViews, Dictionary<long, List<Click>> slidingWindowClicks)
        {
            List<string> viewWithClicks = new List<string>();
            StringBuilder stringBuilder = new StringBuilder();

            foreach (var view in slidingWindowViews) {
                if (slidingWindowClicks.ContainsKey(view.Key))
                {
                    List<Click> clicks = slidingWindowClicks[view.Key];
                    foreach (var click in clicks)
                    {
                        viewWithClicks.Add(
                            stringBuilder
                            .Append(view.Key)
                            .Append(",")
                            .Append(DateTimeFormatting.FormatDateTimeToCustom(view.Value.LogDate))
                            .Append(",")
                            .Append(click.Id).ToString()
                        );
                        stringBuilder.Clear();
                    }
                }
                else
                {
                    viewWithClicks.Add(
                          stringBuilder
                          .Append(view.Key)
                          .Append(",")
                          .Append(DateTimeFormatting.FormatDateTimeToCustom(view.Value.LogDate))
                          .Append(",")
                          .ToString()
                      );
                    stringBuilder.Clear();
                }
            }

            _fileWriter.WriteRows(viewWithClicks, _settings.ViewsWithClicksFilePath);
        }

        private void AggregateViewableViews(Dictionary<long, PartialView> slidingWindowViews, Dictionary<long, ViewableViewEvent> slidingWindowWiewableViewEvents)
        {
            List<string> viewableViews = new List<string>();
            StringBuilder stringBuilder = new StringBuilder();

            foreach (var view in slidingWindowViews)
            {
                if (slidingWindowWiewableViewEvents.ContainsKey(view.Key))
                {
                    ViewableViewEvent viewableView = slidingWindowWiewableViewEvents[view.Key];

                    viewableViews.Add(
                            stringBuilder
                            .Append(view.Key)
                            .Append(",")
                            .Append(DateTimeFormatting.FormatDateTimeToCustom(view.Value.LogDate))
                            .Append(",")
                            .Append(view.Value.CampaignId.ToString())
                            .Append(",")
                            .Append(viewableView.ViewableViewEventId).ToString()
                        );
                    stringBuilder.Clear();

                }
                else
                {
                    viewableViews.Add(
                          stringBuilder
                            .Append(view.Key)
                            .Append(",")
                            .Append(DateTimeFormatting.FormatDateTimeToCustom(view.Value.LogDate))
                            .Append(",")
                            .Append(view.Value.CampaignId.ToString())
                            .Append(",")
                            .ToString()
                      );
                    stringBuilder.Clear();
                }
        }

            _fileWriter.WriteRows(viewableViews, _settings.ViewableViewsFilePath);
        }

        //Dictionary because I want to remove duplicate views & its an easy way to check if such a view already exists
        //Removing duplicates to calclucate more precise statistics
        //if you have two identical views and 1 click, what to do then..
        public Dictionary<long, PartialView> GetSlidingWindowViews()
        {
            Dictionary<long, PartialView> views = new Dictionary<long, PartialView>();
            while (_viewFileReaderSettings.NotallRecordsProccessedInWindow)
            {
                var viewRows = _fileReader.ReadRows(_viewFileReaderSettings);
                if (viewRows.Count() == 0) {
                    _viewFileReaderSettings.EOF = true;
                    break;
                }
                views = ProcessViewRows(viewRows, views);

            }
            _viewFileReaderSettings.NotallRecordsProccessedInWindow = true;
            return views;
        }

        public Dictionary<long, List<Click>> GetSlidingWindowClicks()
        {
            Dictionary<long, List<Click>> clicks = new Dictionary<long, List<Click>>();
            while (_clickFileReaderSettings.NotallRecordsProccessedInWindow)
            {
                var rows = _fileReader.ReadRows(_clickFileReaderSettings);
                if (rows.Count() == 0)
                {
                    break;
                }
                clicks = ProcessClickRows(rows, clicks);

            }
            _clickFileReaderSettings.NotallRecordsProccessedInWindow = true;
            return clicks;
        }

        public Dictionary<long, ViewableViewEvent> GetSlidingWindowViewableViewEvents()
        {
            Dictionary<long, ViewableViewEvent> viewableViewEvents = new Dictionary<long, ViewableViewEvent>();
            while (_viewableViewEventFileReaderSettings.NotallRecordsProccessedInWindow)
            {
                var rows = _fileReader.ReadRows(_viewableViewEventFileReaderSettings);
                if (rows.Count() == 0)
                {
                    break;
                }
                viewableViewEvents = ProcessViewableViewEventRows(rows, viewableViewEvents);

            }
            _viewableViewEventFileReaderSettings.NotallRecordsProccessedInWindow = true;
            return viewableViewEvents;
        }
        
        #region Processing 
        //removing duplicate Views
        private Dictionary<long, PartialView> ProcessViewRows(string[] Rows, Dictionary<long, PartialView> views)
        {
            var RowsAsStringArray = Rows.Select(a=>a.Split(',')).ToArray();
            var rowCount = Rows.Count();
            if (_settings.SlidingWindowLastLogTime == null)
            {
               _settings.SlidingWindowLastLogTime = Convert.ToDateTime(RowsAsStringArray.FirstOrDefault()[1]).AddMinutes(_settings.StreamWindomInMinutes);
            }
            else if(_viewFileReaderSettings.SlidingWindowEndsInCurrentDataSet)
            {
                _settings.SlidingWindowLastLogTime= _settings.SlidingWindowLastLogTime.Value.AddMinutes(_settings.StreamWindomInMinutes);
            }

            var slidingWindowInfo = SlidingWindowEvaluation.Get(RowsAsStringArray.Select(a => Convert.ToDateTime(a[1])).ToArray(), rowCount, _settings.SlidingWindowLastLogTime.Value);
            var slidingWindowEndsInThisDataSet = slidingWindowInfo.Item1;
            _viewFileReaderSettings.SlidingWindowEndsInCurrentDataSet = slidingWindowEndsInThisDataSet;
            var performDateTimeValidationFromRow = slidingWindowInfo.Item2;

            for (var i=0; i<rowCount; i++)
            { 
                PartialView view = new PartialView
                    {
                        LogDate = Convert.ToDateTime(RowsAsStringArray[i][1]),
                        CampaignId = Convert.ToInt32(RowsAsStringArray[i][2])
                    };
                var viewId = Convert.ToInt64(RowsAsStringArray[i][0]);

                if (!views.ContainsKey(viewId))
                {
                    if (slidingWindowEndsInThisDataSet)
                    {

                        if (i >= performDateTimeValidationFromRow)
                        {
                            if (_settings.SlidingWindowLastLogTime.Value < view.LogDate)
                            {
                                _viewFileReaderSettings.Skip += i;
                                _viewFileReaderSettings.NotallRecordsProccessedInWindow = false;
                                break;
                            }
                        }
                    }

                    views.Add(
                        viewId,
                        view
                    );
                }
            }

            if (!slidingWindowEndsInThisDataSet)
            {
                _viewFileReaderSettings.Skip += rowCount;
                _viewFileReaderSettings.NotallRecordsProccessedInWindow = true;
            }

            return views;
        }
        //Each View can be connected to multiple Clicks
        private Dictionary<long, List<Click>> ProcessClickRows(string[] Rows, Dictionary<long, List<Click>> clicks)
        {
            var RowsAsStringArray = Rows.Select(a => a.Split(',')).ToArray();
            var rowCount = Rows.Count();
            DateTime SlidingWindowEndDateTime = _settings.SlidingWindowLastLogTime.Value;
            var slidingWindowInfo = SlidingWindowEvaluation.Get(RowsAsStringArray.Select(a => Convert.ToDateTime(a[1])).ToArray(), rowCount, SlidingWindowEndDateTime);
            var slidingWindowEndsInThisDataSet = slidingWindowInfo.Item1;
            var performDateTimeValidationFromRow = slidingWindowInfo.Item2;

            for (var i = 0; i < rowCount; i++)
            {
                Click click = new Click
                {
                    Id = Convert.ToInt64(RowsAsStringArray[i][0]),
                    LogDate = Convert.ToDateTime(RowsAsStringArray[i][1]),
                    CampaignId = Convert.ToInt32(RowsAsStringArray[i][2]), 
                };

                var viewId = Convert.ToInt64(RowsAsStringArray[i][3]);

                if (!clicks.ContainsKey(viewId))
                {
                    if (slidingWindowEndsInThisDataSet)
                    {
                        if (i >= performDateTimeValidationFromRow)
                        {
                            if (SlidingWindowEndDateTime < click.LogDate)
                            {
                                _clickFileReaderSettings.Skip += i;
                                _clickFileReaderSettings.NotallRecordsProccessedInWindow = false;
                                break;
                            }
                        }
                    }

                    clicks.Add(
                        viewId, new List<Click>() {
                        click}
                    );
                }
                else {
                    clicks[viewId].Add(click);
                }

            }

            if (!slidingWindowEndsInThisDataSet)
            {
                _clickFileReaderSettings.Skip += rowCount;
                _clickFileReaderSettings.NotallRecordsProccessedInWindow = true;
            }

            return clicks;
        }
        //Current data does not provide evidence that for a single View we would have multipe ViewableViewEvents
        private Dictionary<long, ViewableViewEvent> ProcessViewableViewEventRows(string[] Rows, Dictionary<long, ViewableViewEvent> viewableViewEvents)
        {
            var RowsAsStringArray = Rows.Select(a => a.Split(',')).ToArray();
            var rowCount = Rows.Count();
            DateTime SlidingWindowEndDateTime = _settings.SlidingWindowLastLogTime.Value;
            var slidingWindowInfo = SlidingWindowEvaluation.Get(RowsAsStringArray.Select(a => Convert.ToDateTime(a[1])).ToArray(), rowCount, SlidingWindowEndDateTime);
            var slidingWindowEndsInThisDataSet = slidingWindowInfo.Item1;
            var performDateTimeValidationFromRow = slidingWindowInfo.Item2;

            for (var i = 0; i < rowCount; i++)
            {
                ViewableViewEvent viewEvent = new ViewableViewEvent
                {
                    LogDate = Convert.ToDateTime(RowsAsStringArray[i][1]),
                    ViewableViewEventId = Convert.ToInt64(RowsAsStringArray[i][0]),
                };

                var viewId = Convert.ToInt64(RowsAsStringArray[i][2]);

                if (!viewableViewEvents.ContainsKey(viewId))
                {
                    if (slidingWindowEndsInThisDataSet)
                    {
                        if (i >= performDateTimeValidationFromRow)
                        {
                            if (SlidingWindowEndDateTime < viewEvent.LogDate)
                            {
                                _viewableViewEventFileReaderSettings.Skip += i;
                                _viewableViewEventFileReaderSettings.NotallRecordsProccessedInWindow = false;
                                break;
                            }
                        }
                    }
                    viewableViewEvents.Add(viewId, viewEvent);
                }
                else {
                    var a = viewEvent;
                }

            }

            if (!slidingWindowEndsInThisDataSet)
            {
                _viewableViewEventFileReaderSettings.Skip += rowCount;
                _viewableViewEventFileReaderSettings.NotallRecordsProccessedInWindow = true;
            }
            return viewableViewEvents;
        }
        #endregion
    }
}
