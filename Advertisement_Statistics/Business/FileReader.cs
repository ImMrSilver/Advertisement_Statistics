﻿using Advertisement_Statistics.Interfaces;
using Advertisement_Statistics.Models;
using System.IO;
using System.Linq;

namespace Advertisement_Statistics.Business
{
    public class FileReader : IFileReader
    {
        //Pretty sure thre was a better way to do this, but his was a simple way to read data in "batch'es"
        public string[] ReadRows(FileReaderSettings settings){ 
          var Rows = File.ReadLines(settings.FilePath).Skip(settings.Skip).Take(settings.Take).ToArray(); 
          return Rows;
        }


        //some alternatives, really not great..
        public string[] ReadRowsFromStream(FileReaderSettings settings)
        {
            SkippableStreamReader exampleReader = new SkippableStreamReader(settings.FilePath);
            exampleReader.SkipLines(settings.Skip);
            var result = exampleReader.ReadLines(settings.Take);
            return result;
        }

        class SkippableStreamReader : StreamReader
        {
            public SkippableStreamReader(string path) : base(path) { }
            
            //yeah..
            public void SkipLines(int linecount)
            {
                for (int i = 0; i < linecount; i++)
                {
                    //..
                    this.ReadLine();
                }
            }
            public string[] ReadLines(int linecount)
            {
                string[] stringBatch = new string[linecount];

                for (int i = 0; i < linecount; i++)
                {
                    stringBatch[i]=ReadLine();
                }
                return stringBatch;
            }
        }
    }
}
