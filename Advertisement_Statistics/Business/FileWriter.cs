﻿using Advertisement_Statistics.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advertisement_Statistics.Business
{
    public class FileWriter : IFileWriter
    {
        public void WriteRows(List<string> rows, string path)
        {
            string[] split = path.Split('\\');
            string firstPart = string.Join("\\", split.Take(split.Length - 1));
            bool exists = Directory.Exists(firstPart);

            if (!exists)
                Directory.CreateDirectory(firstPart);
            using (var tw = new StreamWriter(path, true))
            {
                foreach (var row in rows)
                {
                    tw.WriteLine(row);
                }
            }
        }
    }
}
