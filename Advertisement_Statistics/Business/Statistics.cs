﻿using Advertisement_Statistics.Common;
using Advertisement_Statistics.Interfaces;
using Advertisement_Statistics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advertisement_Statistics.Business
{
    public class Statistics : IStatistics
    {
        private StatisticsSettings _settings;
        private IFileReader _fileReader;
        private IFileWriter _fileWriter;
        private FileReaderSettings _viewsWithClicksSettings;
        private FileReaderSettings _viewableViewsFilePath;
        public Statistics(
            StatisticsSettings settings,
            IFileReader fileReader,
            IFileWriter fileWriter)
        {
            _fileReader = fileReader;
            _fileWriter = fileWriter;
            _settings = settings;
            SetFileReaderSettings();
        }

        private void SetFileReaderSettings()
        {
            _viewsWithClicksSettings = new FileReaderSettings
            {
                FilePath = _settings.ViewsWithClicksFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };

            _viewableViewsFilePath = new FileReaderSettings
            {
                FilePath = _settings.ViewableViewsFilePath,
                Skip = _settings.Skip,
                Take = _settings.Take
            };
        }

        public void Create()
        {
            Console.WriteLine("Avertisement statistics generation started.");
            var fileContainsRows = !_viewsWithClicksSettings.EOF;
            while (fileContainsRows)
            {
                var slidingWindoViewsWithClicks = GetSlidingWindowViewsWithClicks();
                fileContainsRows = !_viewsWithClicksSettings.EOF;
                var slidingWindowClicks = GetSlidingWindowCampaignViews();
                if (slidingWindowClicks.Count > 0)
                {
                    WriteToFile(slidingWindoViewsWithClicks, slidingWindowClicks);
                }
            }
            Console.WriteLine("Avertisement statistics generated.");
        }

        public void WriteToFile(
            Dictionary<long, List<ViewWithClicks>> slidingWindoViewsWithClicks,
            Dictionary<int, List<CampaignViewStatistics>> slidingWindowCampaignViewss
            )
        {

            List<string> statistics = new List<string>();
            StringBuilder stringBuilder = new StringBuilder();
            var customDateFrom = DateTimeFormatting.FormatDateTimeToCustom(_settings.SlidingWindowLastLogTime.Value.AddMinutes(-_settings.StreamWindomInMinutes));
            var customDateTo = DateTimeFormatting.FormatDateTimeToCustomOnlyTime(_settings.SlidingWindowLastLogTime.Value);
            statistics.Add(
                stringBuilder
                .Append("---Statistics Window: ")
                .Append(customDateFrom)
                .Append(" - ")
                .Append(customDateTo)
                .Append("---")
                .ToString());
            stringBuilder.Clear();
            foreach (var campaign in slidingWindowCampaignViewss)
            {
                var viewCount = 0;
                var clickCount = 0;
                var viewableViewCount = 0;
                double clickThroughRate = 0;

                foreach (var campaignView in campaign.Value)
                {
                    viewCount++;

                    if (campaignView.ViewableViewId!=null) {
                        viewableViewCount++;
                    }
                    if (slidingWindoViewsWithClicks.ContainsKey(campaignView.ViewId))
                    {
                        clickCount += slidingWindoViewsWithClicks[campaignView.ViewId].Count(x => x.ClickId != null);
                    }
                }
                clickThroughRate= Math.Round((double)(clickCount * 100)/viewCount,4);
                statistics.Add(
                            stringBuilder
                            .Append(campaign.Key)
                            .Append(",")
                            .Append(viewCount)
                            .Append(",")
                            .Append(clickCount)
                            .Append(",")
                            .Append(viewableViewCount)
                            .Append(",")
                            .Append(clickThroughRate)
                            .ToString()
                        );
                   stringBuilder.Clear();
            }

            _fileWriter.WriteRows(statistics, _settings.StatisticsFilePath);
        }

        public Dictionary<long, List<ViewWithClicks>> GetSlidingWindowViewsWithClicks()
        {
            Dictionary<long, List<ViewWithClicks>> views = new Dictionary<long, List<ViewWithClicks>>();
            while (_viewsWithClicksSettings.NotallRecordsProccessedInWindow)
            {
                var viewRows = _fileReader.ReadRows(_viewsWithClicksSettings);
                if (viewRows.Count() == 0) {
                    _viewsWithClicksSettings.EOF = true;
                    break;
                }
                views = ProcessViewWithClicksRows(viewRows, views);
            }
            _viewsWithClicksSettings.NotallRecordsProccessedInWindow = true;
            return views;
        }

        public Dictionary<int, List<CampaignViewStatistics>> GetSlidingWindowCampaignViews()
        {
            Dictionary<int, List<CampaignViewStatistics>> campaignViews = new Dictionary<int, List<CampaignViewStatistics>>();
            while (_viewableViewsFilePath.NotallRecordsProccessedInWindow)
            {
                var rows = _fileReader.ReadRows(_viewableViewsFilePath);
                if (rows.Count() == 0)
                {
                    break;
                }
                campaignViews = ProcessCampaignViewRows(rows, campaignViews);

            }
            _viewableViewsFilePath.NotallRecordsProccessedInWindow = true;
            return campaignViews;
        }

        #region Processing 
        private Dictionary<long, List<ViewWithClicks>> ProcessViewWithClicksRows(string[] Rows, Dictionary<long, List<ViewWithClicks>> views)
        {
            var RowsAsStringArray = Rows.Select(a=>a.Split(',')).ToArray();
            var rowCount = Rows.Count();
            if (_settings.SlidingWindowLastLogTime == null)
            {
                _settings.SlidingWindowLastLogTime = Convert.ToDateTime(RowsAsStringArray.FirstOrDefault()[1]).AddMinutes(_settings.StreamWindomInMinutes);
            }
            else if (_viewsWithClicksSettings.SlidingWindowEndsInCurrentDataSet)
            {
                _settings.SlidingWindowLastLogTime = _settings.SlidingWindowLastLogTime.Value.AddMinutes(_settings.StreamWindomInMinutes);
            }

            var slidingWindowInfo = SlidingWindowEvaluation.Get(RowsAsStringArray.Select(a => Convert.ToDateTime(a[1])).ToArray(), rowCount, _settings.SlidingWindowLastLogTime.Value);
            var slidingWindowEndsInThisDataSet = slidingWindowInfo.Item1;
            _viewsWithClicksSettings.SlidingWindowEndsInCurrentDataSet = slidingWindowEndsInThisDataSet;
            var performDateTimeValidationFromRow = slidingWindowInfo.Item2;

            for (var i = 0; i < rowCount; i++)
            {
                long? ClickId = null;
                if (!String.IsNullOrEmpty(RowsAsStringArray[i][2]))
                {
                    ClickId = Convert.ToInt64(RowsAsStringArray[i][2]);
                }
                ViewWithClicks viewClick = new ViewWithClicks
                {
                    LogDate = Convert.ToDateTime(RowsAsStringArray[i][1]),
                    ClickId = ClickId
                };
                var viewId = Convert.ToInt64(RowsAsStringArray[i][0]);

                if (!views.ContainsKey(viewId))
                {
                    if (slidingWindowEndsInThisDataSet)
                    {
                        if (i >= performDateTimeValidationFromRow)
                        {
                            if (_settings.SlidingWindowLastLogTime < viewClick.LogDate)
                            {

                                _viewsWithClicksSettings.Skip += i;
                                _viewsWithClicksSettings.NotallRecordsProccessedInWindow = false;
                                break;
                            }
                        }
                    }

                    views.Add(
                        viewId,
                        new List<ViewWithClicks>() {
                            viewClick
                        }
                    );
                }
                else
                {

                    if (slidingWindowEndsInThisDataSet)
                    {

                        if (i >= performDateTimeValidationFromRow)
                        {
                            if (_settings.SlidingWindowLastLogTime < viewClick.LogDate)
                            {
                                _viewsWithClicksSettings.Skip += i;
                                _viewsWithClicksSettings.NotallRecordsProccessedInWindow = false;
                                break;
                            }
                        }
                    }
                    views[viewId].Add(
                        viewClick
                );

                }
            }
            if (!slidingWindowEndsInThisDataSet)
            {
                _viewsWithClicksSettings.Skip += rowCount;
                _viewsWithClicksSettings.NotallRecordsProccessedInWindow = true;
            }

            return views;
        }

        private Dictionary<int, List<CampaignViewStatistics>> ProcessCampaignViewRows(
            string[] Rows,
            Dictionary<int, List<CampaignViewStatistics>> campaignViews)
        {
            var RowsAsStringArray = Rows.Select(a => a.Split(',')).ToArray();
            var rowCount = Rows.Count(); 
            var slidingWindowInfo = SlidingWindowEvaluation.Get(RowsAsStringArray.Select(a => Convert.ToDateTime(a[1])).ToArray(), rowCount, _settings.SlidingWindowLastLogTime.Value);
            var slidingWindowEndsInThisDataSet = slidingWindowInfo.Item1;
            var performDateTimeValidationFromRow = slidingWindowInfo.Item2;

            for (var i = 0; i < rowCount; i++)
            {

                long? ViewableViewId = null;
                if (!String.IsNullOrEmpty(RowsAsStringArray[i][3]))
                {
                    ViewableViewId = Convert.ToInt64(RowsAsStringArray[i][3]);
                }

                CampaignViewStatistics campaignView = new CampaignViewStatistics
                {
                    LogDate = Convert.ToDateTime(RowsAsStringArray[i][1]),
                    ViewId = Convert.ToInt64(RowsAsStringArray[i][0]),
                    ViewableViewId = ViewableViewId
                };

                var campaignId = Convert.ToInt32(RowsAsStringArray[i][2]);

                if (!campaignViews.ContainsKey(campaignId))
                {
                    if (slidingWindowEndsInThisDataSet)
                    {
                        if (i >= performDateTimeValidationFromRow)
                        {
                            if (_settings.SlidingWindowLastLogTime.Value < campaignView.LogDate)
                            {
                                _viewableViewsFilePath.Skip += i;
                                _viewableViewsFilePath.NotallRecordsProccessedInWindow = false;
                                break;
                            }
                        }
                    }

                    campaignViews.Add(
                        campaignId, 
                        new List<CampaignViewStatistics>(){
                            campaignView
                        }
                    );
                }
                else
                {
                    if (slidingWindowEndsInThisDataSet)
                    {
                        if (i >= performDateTimeValidationFromRow)
                        {
                            if (_settings.SlidingWindowLastLogTime.Value < campaignView.LogDate)
                            {
                                _viewableViewsFilePath.Skip += i;
                                _viewableViewsFilePath.NotallRecordsProccessedInWindow = false;
                                break;
                            }
                        }
                    }
                    campaignViews[campaignId].Add(campaignView);
                }
            }

            if (!slidingWindowEndsInThisDataSet)
            {
                _viewableViewsFilePath.Skip += rowCount;
                _viewableViewsFilePath.NotallRecordsProccessedInWindow = true;
            }

            return campaignViews;
        }
        #endregion
    }
}

